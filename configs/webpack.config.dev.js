const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const DotenvPlugin = require('dotenv-webpack');

const commonConfig = require('./webpack.config.common');

module.exports = merge(commonConfig, {
  plugins: [
    new FriendlyErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new DotenvPlugin({
      path: path.resolve(__dirname, '../.env.dev'),
    }),
  ],
  devServer: {
    compress: true,
    https: process.env.HTTPS || false,
    port: process.env.PORT || 8001,
    hot: true,
    watchContentBase: true,
  },
});
