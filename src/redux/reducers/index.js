import { combineReducers } from 'redux';

import exportPdf from './export-pdf.reducer';

const reducers = combineReducers({
  // mettre ici les reducers qu'on veut ajouter au store
  exportPdf,
});

export default reducers;
