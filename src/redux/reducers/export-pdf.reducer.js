import { exportActions } from '../../data/action-types';

const initialState = {
  status: '',
};

const exportPdfReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case exportActions.SET_STATUS:
      return {
        ...state,
        status: payload,
      };
    default:
      return state;
  }
};

export default exportPdfReducer;
