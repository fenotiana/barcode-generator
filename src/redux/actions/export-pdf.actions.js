import { exportActions } from '../../data/action-types';
import { exportStatus } from '../../data/export-status';
import pdfService from '../../services/applying/pdf.service';

export const printPdf = exportOptions => async (dispatch) => {
  dispatch({
    type: exportActions.SET_STATUS,
    payload: exportStatus.LOADING,
  });

  const success = await pdfService.generateAndExportQrCodes(exportOptions);
  const status = success ? exportStatus.SUCCESS : exportStatus.FAILURE;

  dispatch({
    type: exportActions.SET_STATUS,
    payload: status,
  });
};
