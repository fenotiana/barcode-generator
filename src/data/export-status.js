export const exportStatus = {
  LOADING: 'loading',
  SUCCESS: 'success',
  FAILURE: 'failure',
};
