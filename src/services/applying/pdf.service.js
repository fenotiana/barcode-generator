import QrCode from 'qrcode';
import JsPdf from 'jspdf';

class PdfService {
  generateAndExportQrCodes = async (exportOptions) => {
    const { qrCodeText } = exportOptions;
    const doc = new JsPdf();
    const qrCodes = await this.generateQRCodes(exportOptions);
    const results = await Promise.all(qrCodes);

    if (Array.isArray(results)) {
      const heigth = doc.internal.pageSize.getHeight();
      const width = doc.internal.pageSize.getWidth();
      const startX = 16;
      const startY = 18;
      const endX = width - 12;
      const endY = heigth - 9;
      let x = 0;
      let y = 20;
      let text = '';

      results.forEach((dataUrl, i) => {
        x = i * 30;
        if ((x % 180) < 30 && i !== 0) {
          y += 30;
          x = 0;
        }

        if (y >= 270) {
          doc.addPage();
          y = 20;
        }

        text = `${qrCodeText} ${i + 1}`;

        doc.setFontSize(10);
        doc.addImage(dataUrl, 'PNG', (x % 180) + 20, y, 25, 25);
        doc.text(text, (x % 180) + (33 - text.length), y + 26);

        // Vertical lines
        doc.line(startX, startY, startX, endY);
        doc.line((x % 180) + 48, 18, (x % 180) + 48, heigth - 9);

        // Horizontal Lines
        doc.line(startX, startY, endX, startY);
        doc.line(startX, y + 28, endX, y + 28);
      });


      doc.save(`${qrCodeText && `${qrCodeText}_`}${results.length}_qr_code.pdf`);

      return true;
    }

    return false;
  }

  generateQRCodes = async ({ qrCodeText, quantity }) => (
    Array.from({ length: quantity }, (_, index) => (
      QrCode.toDataURL(`${qrCodeText}${index + 1}`)
    ))
  )
}

export default new PdfService();
