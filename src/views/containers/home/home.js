import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Spinner from 'react-bootstrap/Spinner';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import * as exportPdfActions from '../../../redux/actions/export-pdf.actions';
import { exportStatus } from '../../../data/export-status';

class Home extends Component {
  state = {
    qrCodeText: '',
    quantity: 0,
  }

  handleTextChange = (event) => {
    const { target } = event;

    this.setState({ [target.name]: target.value });
  }

  handleGenerate = async () => {
    const { quantity, qrCodeText } = this.state;

    if (quantity) {
      const { printPdf } = this.props;

      printPdf({ qrCodeText, quantity });
    }
  }


  render() {
    const { qrCodeText, quantity } = this.state;
    const { status } = this.props;

    return (
      <div className="blc-container">
        <Card.Title>QR Code Generator</Card.Title>
        <div className="blc-form">
          <Form.Control
            value={qrCodeText}
            name="qrCodeText"
            placeholder="Préfixe"
            onChange={this.handleTextChange}
          />
          <Form.Control
            value={quantity}
            name="quantity"
            type="number"
            placeholder="Nombre"
            onChange={this.handleTextChange}
          />
          <Button onClick={this.handleGenerate}>Générer</Button>
          <div style={{ height: '20px' }}>
            {
              status === exportStatus.LOADING ? (
                <Spinner animation="grow" />
              ) : <></>
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateFromProps = state => ({
  status: state.exportPdf.status,
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    ...exportPdfActions,
  }, dispatch),
});

Home.propTypes = {
  printPdf: PropTypes.func,
  status: PropTypes.string,
};

Home.defaultProps = {
  printPdf: () => { },
  status: '',
};

export default connect(mapStateFromProps, mapDispatchToProps)(Home);
