const config = process.env.NODE_ENV === 'development' ? (
  require('./configs/webpack.config.dev')
) : (require('./configs/webpack.config.prod'));

module.exports = config;
