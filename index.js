import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import Main from './src/main';
import './src/assets/scss/index.scss';

ReactDOM.render(
  <Main />,
  document.getElementById('root'),
);
